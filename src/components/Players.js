import React from "react";
import api from "../action/apis";
import Autosuggest from 'react-autosuggest';

const dateFormat = require('dateformat');
const now = new Date();
class Players extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: true,
            playerList: null,
            playersImages: {},
            dummImages: [],
            autoSuggest: true,
            value: "",
            suggestions: []
        }
    }
    componentDidMount = async () => {
        const playersData = await api.getPlayersList();
        const images = await this.importAllImages(require.context('../player-images', true, /\.jpg$/));
        let imageArray = [];
        Object.keys(images).map(async img => {
            imageArray.push({
                url: images[img].default,
                id: img.split(".")[0]
            })
        })
        this.setState({
            playerList: playersData.playerList,
            dummImages: imageArray,
            loader: false
        })
    }
    //Importing all images from folder.
    importAllImages = (r) => {
        let images = {};
        r.keys().map((item, index) => {
            images[item.replace('./', '')] = r(item);
        });
        return images;
    }
    escapeRegexCharacters = (str) => {
        return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }
    getSuggestions = (value) => {
        const escapedValue = this.escapeRegexCharacters(value.trim());
        if (escapedValue === "") {
            return [];
        }
        const regex = new RegExp("^" + escapedValue, "i");
        return this.state.playerList.filter((list) => regex.test(list.TName || list.PFName));
    }
    getSuggestionValue = (suggestion) => {
        return suggestion.name;
    }
    renderSuggestion = (suggestion) => {
        return <span>{suggestion.name}</span>;
    }
    onChange = (event, { newValue, method }) => {
        this.setState({
            value: newValue
        });
    };
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            playerList: this.getSuggestions(value)
        });
    };
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };
    render = () => {
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: "Type Player name here..",
            value,
            onChange: this.onChange
        };
        return (
            <>
                <h1>Search Bar</h1>
                {
                    this.state.autoSuggest &&
                    <Autosuggest
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                        getSuggestionValue={this.getSuggestionValue}
                        renderSuggestion={this.renderSuggestion}
                        inputProps={inputProps}
                    />
                }
                {
                    !this.state.loader ?
                        this.state.playerList.map((player, index) => {
                            return (
                                <div className="card" key={index} style={{
                                    display: 'inline-block',
                                    border: '1px solid #eee',
                                    boxShadow: "0 2px 2px #ccc",
                                    width: "300px",
                                    padding: "20px",
                                    margin: "20px"
                                }}>
                                    {
                                        (this.state.dummImages).map((key, idx) => {
                                            if (player.Id == key.id) {
                                                return (
                                                    <>
                                                        <img style={{ width: "150px" }} src={key.url} key={idx} />
                                                    </>
                                                )
                                            }
                                        })
                                    }
                                   Player Name:- {player.PFName}<br />
                                   Skill:- {player.SkillDesc}<br />
                                   Value :- $&nbsp;{player.Value}<br />
                                   Upcoming Match :- {player.UpComingMatchesList[0].CCode + " vs " + player.UpComingMatchesList[0].VsCCode}<br />
                                   Next Match :- {dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")}
                                </div>
                            )
                        })
                        :
                        null//loader will here....
                }
            </>
        )
    }
}
export default Players;