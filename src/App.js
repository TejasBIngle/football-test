import './App.css';
import Players from '../src/components/Players';
import SearchBar from '../src/components/SearchBar';

function App() {
  return (
    <>
      {/* <SearchBar /> */}
      <Players />
    </>
  )
}
export default App;
