var axios = require('axios');
var confign = require('../config.json')

const ApiService = {
    getPlayersList: async function (req) {
        const config = {
            method: 'get',
            url: confign.apiUrl
        }
        let data = await axios(config);
        let finalresp = data;
        return finalresp.data
    }
}
export default ApiService;

